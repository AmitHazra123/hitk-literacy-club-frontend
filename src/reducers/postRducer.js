import {
  GET_ALL_POSTS,
  GET_POST,
  UPLOAD_POST,
  UPDATE_POST,
  LIKE_POST,
  DELETE_POST
} from "../actions/types";

const initialState = {
  posts: [],
  post: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_POSTS:
      return {
        ...state,
        posts: action.payload
      };
    case GET_POST:
      return {
        ...state,
        post: action.payload
      };
    case UPLOAD_POST:
      return Object.assign({}, state.posts, action.payload);
    case UPDATE_POST:
      return Object.assign({}, state.posts, action.payload);
    case LIKE_POST:
      return Object.assign({}, state.posts, action.payload);
    case DELETE_POST:
      return state.posts.filter(post => post._id !== action.payload);
    default:
      return state;
  }
}
