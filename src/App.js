import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";
import { Provider } from "react-redux";
import "./App.css";
import store from "./store";
import LandingPage from "./components/LandingPage";
import Post from "./components/Post";
import Header from "./components/Header";
import Login from "./components/Login";
import AdminPage from "./components/AdminPage";
import AboutPage from "./components/AboutPage";
import UpdatePost from "./components/UpdatePost";
import InsertPost from "./components/InsertPost";

const history = createBrowserHistory();

function App() {
  return (
    <div className="App">
      <Header />
      <div className="body">
        <Provider store={store}>
          <Router history={history}>
            <Route exact path="/" component={LandingPage} />
            <Route exact path="/post/:id" component={Post} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/admin" component={AdminPage} />
            <Route exact path="/about" component={AboutPage} />
            <Route exact path="/insert" component={InsertPost} />
            <Route exact path="/update/:id" component={UpdatePost} />
          </Router>
        </Provider>
      </div>
    </div>
  );
}

export default App;
