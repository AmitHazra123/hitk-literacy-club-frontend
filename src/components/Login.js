import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { login } from "../actions/auth";

class Login extends Component {
  componentWillReceiveProps(props) {
    console.log(props.error);
  }

  render() {
    return <div>Login</div>;
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error
});

export default connect(mapStateToProps, { login })(Login);
