import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getAllPosts, likePost } from "../actions/post";

class LandingPage extends Component {
  componentDidMount() {
    this.props.getAllPosts();
  }

  componentWillReceiveProps(props) {
    console.log(props.posts);
  }

  render() {
    return <div>Home Page</div>;
  }
}

LandingPage.propTypes = {
  posts: PropTypes.array.isRequired,
  error: PropTypes.object.isRequired,
  getAllPosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  posts: state.postReducer.posts,
  error: state.error
});

export default connect(mapStateToProps, { getAllPosts, likePost })(LandingPage);
