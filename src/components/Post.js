import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getPost } from "../actions/post";

class Post extends Component {
  componentDidMount() {
    const postId = this.props.match.params.id;
    this.props.getPost(postId);
  }

  componentWillReceiveProps(props) {
    console.log(props.post);
  }

  render() {
    return <div>Post Details</div>;
  }
}

Post.propTypes = {
  post: PropTypes.object.isRequired,
  getPost: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  post: state.postReducer.post
});

export default connect(mapStateToProps, { getPost })(Post);
